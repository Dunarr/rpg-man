import './style.css'
import {loadSprites} from "./spriteManager.js";
import Player from "./src/Player.js";
import {generateMap} from "./src/generateMap.js";
import collide from "./src/collide.js";
import selectLevel from "./src/levelSelector.js";
import {Howl} from "howler"
import menuThemeWav from './assets/music/MenuTheme.wav'
import coinWave from './assets/music/coin.wav'
import players from './assets/sprites/players.png'
import zombie from './assets/sprites/zombie.png'
import torch_key_gems from './assets/sprites/torch_key_gems.png'
import lightblue_bricks from './assets/sprites/lightblue_bricks.png'

const music = new Howl({
    src: [menuThemeWav]
})
music.volume(0.3)

const coinEffect = new Howl({
    src: [coinWave]
})


loadSprites({
    players: players,
    zombie: zombie,
    item: torch_key_gems,
    wall: lightblue_bricks,
})
    .then(selectLevel)
    .then(level => {

        let score = 0

        music.play()
        const canvas = document.getElementById('app')
        canvas.height = 1080
        canvas.width = 1920
        const ctx = canvas.getContext('2d')
        ctx.font = '40px sans-serif'
        ctx.fillStyle = '#FBEC44'
        ctx.imageSmoothingEnabled = false;

        const joueur = new Player();

        joueur.x = 64;
        joueur.y = 64;


        const elements = generateMap(level)


        window.addEventListener('keypress', function (e) {
            console.log(e.key);
            switch (e.key) {
                case 'z':
                    joueur.wantedDirection = 'UP';
                    break;
                case 's':
                    joueur.wantedDirection = 'DOWN';
                    break;
                case 'q':
                    joueur.wantedDirection = 'LEFT';
                    break;
                case 'd':
                    joueur.wantedDirection = 'RIGHT';
                    break;
            }
        })

        function updateGame() {
            ctx.clearRect(0, 0, 1920, 1080)
            elements.coins.forEach((coin, i) => {
                if (collide(joueur, coin)) {
                    score ++
                    elements.coins.splice(i, 1)
                    coinEffect.play()
                }
                coin.update(ctx)
            })

            const x = joueur.x
            const y = joueur.y
            joueur.update(ctx);
            let canTurn = true
            elements.walls.forEach(wall => {
                if (collide(joueur, wall)) {
                    joueur.x = x
                    joueur.y = y
                }
                const wantedPos = joueur.getNextPos(joueur.wantedDirection)
                if (collide(wantedPos, wall)) {
                    canTurn = false
                }
                wall.update(ctx)
            })
            if (canTurn) {
                joueur.direction = joueur.wantedDirection
            }
            ctx.fillText(score, 12, 52)


            requestAnimationFrame(updateGame)
        }

        updateGame()
        setInterval(() => {
            joueur.updateSprite()
        }, 200)
    })


    .then(result => console.log(result))
