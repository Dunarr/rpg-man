import {getSprite} from "../spriteManager.js";

export default class Coin {
    x;
    y;

    constructor() {
        this.x = 0;
        this.y = 0;
    }

    update(ctx){
        ctx.drawImage(getSprite('item'), 16, 16, 16, 16, this.x, this.y, 64, 64)
    }
}
