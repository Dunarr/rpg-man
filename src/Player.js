import {getSprite} from "../spriteManager.js";

export default class Player {
    x;
    y;
    direction;
    wantedDirection;
    sprite;

    constructor() {
        this.x = 0;
        this.y = 0;
        this.direction = 'DOWN';
        this.wantedDirection = 'DOWN';
        this.sprite = 0;
    }

    updateSprite(){
        if (this.sprite === 0){
            this.sprite = 16
        } else {
            this.sprite = 0
        }
    }


    getNextPos(direction){
        let x = this.x
        let y = this.y
        switch (direction){
            case 'DOWN':
                y += 2;
                break;
            case 'UP':
                y -= 2;
                break;
            case 'RIGHT':
                x+=2;
                break
            case 'LEFT':
                x-=2;
        }

        if(this.y > 1080){
            y = 0
        }
        if (this.y < 0){
            y = 1080
        }
        if (this.x > 1920){
            x = 0
        }
        if (this.x < 0){
            x = 1920
        }
        return {x, y}
    }

    update(ctx){
        const {x, y} =this.getNextPos(this.direction)
        this.x = x
        this.y = y

        let spritePos = 0
        switch (this.direction) {
            case 'UP':
                spritePos = 32
                break
            case 'RIGHT':
                spritePos = 64;
                break
            case 'DOWN':
                spritePos = 0;
                break
            case 'LEFT':
                spritePos = 96;
                break

        }


        ctx.drawImage(getSprite('players'), spritePos + this.sprite, 0, 16, 16, this.x, this.y, 64, 64)
    }
}
