export default function collide(player, element) {
    return player.x + 64 > element.x && player.x < element.x + 64 && player.y + 64 > element.y && player.y < element.y + 64
}
