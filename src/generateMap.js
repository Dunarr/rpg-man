import Coin from "./Coin.js";
import Wall from "./Wall.js";

export function generateMap(level){
    const elements ={
        coins: [],
        walls: [],
    }
    for (let x = 0; x < 30; x+=1){
        for (let y = 0; y < 17; y+=1) {
            if(level[y][x] === 0){
                const coin = new Coin()
                coin.y = y*64
                coin.x = x*64
                elements.coins.push(coin)
            }
            if(level[y][x] === 1){
                const wall = new Wall()
                wall.y = y*64
                wall.x = x*64
                elements.walls.push(wall)
            }
        }
    }
    return elements
}
