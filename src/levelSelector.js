import level1 from '../levels/level1.json'
import level2 from '../levels/level2.json'

const levels = [
    {
        name: "Test",
        content: level1
    },
    {
        name: "Premier niveau",
        content: level2
    }
]
export default function selectLevel(){
    const menu = document.createElement("div")
    menu.classList.add("menu")
    document.body.appendChild(menu)
    return new Promise(resolve => {
        levels.forEach(level =>  {
            const button = document.createElement("button")
            button.innerHTML = level.name
            button.addEventListener("click", ()=>{
                document.body.removeChild(menu)
                resolve(level.content)
            })
            menu.appendChild(button)
        })
    })
}
